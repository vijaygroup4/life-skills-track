- The **Feynman Technique** posits that articulating and explaining a learned concept to others can significantly enhance one's understanding of the material.
- Thomas Edison held ball bearings in his hands and contemplated the problem.
- **Active thinking** is when you are focused, consciously concentrating on a specific task or idea.
- **Diffused thinking** is more relaxed, your mind wanders, and it's a state where creative insights and connections can occur.
- Points:
  1. Deconstruct the skill.
  2. Learn enough to self-correct.
  3. Remove practice barriers.
  4. Practice at least 20 hours.
- ### Actions to Improve Learning Process

  1. **Engage with Full Attention:**

     - Perform tasks with 100% involvement.
     - Recognize that programming requires sustained and concentrated attention.

  2. **Limit Social Media Distractions:**

     - Block social media during work hours using tools like TimeLimit, Freedom, etc.
     - Enhance productivity by tracking time with apps like Boosted.

  3. **Learning Mindset:**

     - Treat deadlines as tools for time allocation.
     - Approach learning with curiosity and joy.
     - Make the learning process enjoyable and simple.

  4. **Evaluation Criteria:**

     - Focus on understanding concepts well, demonstrating verbal and/or written explanations.
     - Implement concepts clearly in code.
     - Work intensively and meet allotted timeframes.
     - Proactively communicate obstacles and difficulties.
     - Communicate clearly with precise words.

  5. **Continuous Improvement:**

     - Regularly evaluate and enhance the learning process.
     - Seek improvement in verbal and written communication, coding clarity, and timely completion of tasks.
