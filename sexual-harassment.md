### **Types of Behavior Leading to Sexual Harassment**

Sexual harassment can manifest in various forms, including but not limited to:

### **Verbal Harassment:**

Verbal harassment involves the use of spoken or written words that are offensive, intimidating, or sexually explicit. This can include:

- **Offensive Jokes:** Telling jokes that are inappropriate, offensive, or sexually suggestive in nature.
- **Comments:** Making explicit or derogatory comments about a person's appearance, clothing, or private life.
- **Explicit Language:** Using vulgar or sexually explicit language that creates discomfort or distress for the recipient.

### **Visual Harassment:**

Visual harassment encompasses unwelcome actions that involve images, gestures, or any visual representation. Examples include:

- **Inappropriate Gestures:** Making lewd or offensive gestures that target an individual or a group.
- **Display of Explicit Materials:** Showing sexually explicit images, videos, or materials without consent.
- **Staring or Gazing:** Persistent and unwarranted staring that creates discomfort or a hostile environment.

### **Physical Harassment:**

Physical harassment involves any unwanted physical contact or actions that can be intimidating or harmful. This includes:

- **Unwanted Touching:** Inappropriate physical contact without clear and voluntary consent.
- **Blocking or Impeding:** Intentionally blocking someone's path, impeding their movement, or invading personal space.
- **Forced Physical Interaction:** Coercing someone into physical interactions against their will.

### **Responding to Sexual Harassment**

If you face or witness any incident or repeated incidents of sexual harassment, consider taking the following steps:

1. **Ensure Safety:**

   - If immediate danger is present, prioritize the safety of the victim and yourself. If necessary, contact authorities.

2. **Document the Incident:**

   - Record details such as date, time, location, and any involved parties. Note specific behaviors and gather evidence if possible.

3. **Report to Authorities:**

   - If the harassment occurs in a workplace or educational institution, report the incident to the appropriate authorities, such as HR, a supervisor.

4. **Support the Victim:**

   - If you're a witness, offer support to the victim. Encourage them to report the incident and seek professional help if needed.
